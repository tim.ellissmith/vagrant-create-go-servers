# Go Webservers behind and Nginx LoadBalancer #

This repo is designed to use Vagrant to auto install a Jenkins Build Server, an Nginx Server and two Go Application servers.

Running the runme.sh script will initialise the four Vagrant boxes and start the install process.

## Requirements ##
 - Vagrant
 - Desktop / server capable of supporting four VMs


## Usage ##
Run the runthis.sh script.

## Design ##
Currently the Nginx server uses Puppet while the other servers use bootstrapping as defined in the Vagrant file.

The runme.sh script firstly initialises the Jenkins instance and copies the build files to the server; then initialises the Nginx server before initialising the two Go Application servers.

It then curls the Jenkins server to run the build job, which deploys the application on both the Go servers.

Once this is done, it curls the Nginx server to check if the webserver is running correctly.

Finally it compares the outputs of two consecutive curl commands to see if they return results from different go-servers returning success if they are different.

## Security ##
There is absolutely no security build into this system as it is designed to be run internally only.
This means that there are no firewalls, SSH is done over password and Jenkins has no authentication.

You have been warned!

## Possible enhancements ##
 - The scripts should be updated so the entire system uses Puppet
 - Security needs to be build in
 - A version needs to be build on Terraform to allow this to be used on AWS / OpenStack
 - Build a full init script for the Go-Servers
