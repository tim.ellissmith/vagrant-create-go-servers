# -*- mode: ruby -*-
# vi: set ft=ruby :

Vagrant.configure(2) do |config|

  config.vm.define "web" do |web|
    web.vm.box = "puppetlabs/ubuntu-14.04-64-puppet"
    web.vm.network  "private_network", ip: "192.168.56.101"
    web.vm.network "forwarded_port", guest: 80, host: 8000
    web.vm.provider "virtualbox" do |vb|
      vb.memory = "512"
      vb.cpus = "1"
    end
    web.vm.hostname = "web.example.com"
    web.vm.provision "shell", inline: <<-SHELL
      apt-get update
      puppet apply --modulepath=/vagrant/modules/ /vagrant/manifests/site.pp
    SHELL
  end

  config.vm.define "app01" do |app01|
    app01.vm.box = "puppetlabs/ubuntu-14.04-64-puppet"
    app01.vm.network  "private_network", ip: "192.168.56.102"
    app01.vm.provider "virtualbox" do |vb|
      vb.memory = "512"
      vb.cpus = "1"
    end
    app01.vm.hostname = "app01.example.com"  
    app01.vm.provision "shell", inline: <<-SHELL
      apt-get update
      puppet apply --modulepath=/vagrant/modules/ /vagrant/manifests/site.pp
      useradd jenkins --home-dir /home/jenkins --create-home --skel /etc/skel --shell /bin/bash 
      echo "jenkins:l7Bffo3$" | chpasswd
      mkdir /go-servers
      mkdir /var/log/go-servers
      chown jenkins.jenkins /go-servers
      chown jenkins.jenkins /var/log/go-servers
      cp /vagrant/go-server/go.sh /go-servers
      chown jenkins.jenkins /go-servers/go.sh
    SHELL
  end

  config.vm.define "app02" do |app02|
    app02.vm.box = "puppetlabs/ubuntu-14.04-64-puppet"
    app02.vm.network  "private_network", ip: "192.168.56.103"
    app02.vm.provider "virtualbox" do |vb|
      vb.memory = "512"
      vb.cpus = "1"
    end
    app02.vm.hostname = "app02.example.com"  
    app02.vm.provision "shell", inline: <<-SHELL
      apt-get update
      puppet apply --modulepath=/vagrant/modules/ /vagrant/manifests/site.pp
      useradd jenkins --home-dir /home/jenkins --create-home --skel /etc/skel --shell /bin/bash 
      echo "jenkins:l7Bffo3$" | chpasswd
      mkdir /go-servers
      mkdir /var/log/go-servers
      chown jenkins.jenkins /var/log/go-servers
      chown jenkins.jenkins /go-servers
      cp /vagrant/go-server/go.sh /go-servers
      chown jenkins.jenkins /go-servers/go.sh
    SHELL
  end


  config.vm.define "jenkins" do |jenkins|
    jenkins.vm.box = "puppetlabs/ubuntu-14.04-64-puppet"
    jenkins.vm.network  "private_network", ip: "192.168.56.104"
    jenkins.vm.network "forwarded_port", guest: 8080, host: 8080
    jenkins.vm.provider "virtualbox" do |vb|
      vb.memory = "1024"
      vb.cpus = "2"
    end
    jenkins.vm.hostname = "jenkins.example.com"
    jenkins.vm.provision "shell", inline: <<-SHELL
     apt-get update
     wget -q -O - https://jenkins-ci.org/debian/jenkins-ci.org.key | sudo apt-key add -
     sh -c 'echo deb http://pkg.jenkins-ci.org/debian binary/ > /etc/apt/sources.list.d/jenkins.list'
     apt-get update -y
     apt-get install jenkins -y
     apt-get install git -y
     apt-get install -y sshpass
     wget -P /var/lib/jenkins/plugins/ https://updates.jenkins-ci.org/latest/golang.hpi
     wget -P /var/lib/jenkins/plugins/ https://updates.jenkins-ci.org/latest/git-client.hpi
     chown -R jenkins.jenkins /var/lib/jenkins/plugins
     wget https://storage.googleapis.com/golang/go1.6.linux-amd64.tar.gz
     tar -C /usr/local -xzf go1.6.linux-amd64.tar.gz
     echo export PATH=$PATH:/usr/local/go/bin >> /etc/profile
     cp -r /vagrant/Go-Webserver /var/lib/jenkins/jobs
     chown -R jenkins.jenkins /var/lib/jenkins/jobs
     service jenkins restart
   SHELL
  end
end
