#!/bin/bash

if [ $(ps aux | grep webserver | wc -l ) -eq 2 ]; then RUNNING=1; else RUNNING=0; fi

function stop {
# Terminate the service if it isn't running
  if [ $RUNNING -eq 1 ]; then echo "Stopping the web server"; killall webservers; else echo "Webserver is not running"; fi
}


function start {
# Start the webserver
  if [ $RUNNING -eq 0 ]; then echo "Starting the webserver"; nohup sh -c '/go-servers/webservers < /dev/null > /var/log/go-servers/go-servers.log &'  ; echo -e "\n"; else echo "Webserver is already running"; fi
}


case $1 in 
  "start")  
     start
     ;;

  "stop")
    stop
    ;;
  "restart")
    stop;
    start 
  ;;

  *)
   echo "usage $0 <start|stop|restart>"
  ;;
esac

