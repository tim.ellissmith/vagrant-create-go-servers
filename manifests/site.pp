
## site.pp ##

# This file (/etc/puppetlabs/puppet/manifests/site.pp) is the main entry point
# used when an agent connects to a master and asks for an updated configuration.
#
# Global objects like filebuckets and resource defaults should go in this file,
# as should the default node definition. (The default node can be omitted
# if you use the console and don't define any other nodes in site.pp. See
# http://docs.puppetlabs.com/guides/language_guide.html#nodes for more on
# node definitions.)

## Active Configurations ##

# PRIMARY FILEBUCKET
# This configures puppet agent and puppet inspect to back up file contents when
# they run. The Puppet Enterprise console needs this to display file contents
# and differences.

# Define filebucket 'main':

# Make filebucket 'main' the default backup location for all File resources:

# DEFAULT NODE
# Node definitions in this file are merged with node data from the console. See
# http://docs.puppetlabs.com/guides/language_guide.html#nodes for more on
# node definitions.

# The default node definition matches any node lacking a more specific node
# definition. If there are no other nodes in this file, classes declared here
# will be included in every node's catalog, *in addition* to any classes
# specified in the console for that node.

# -----------------------------------------------------------------------------
# Readme for Go-Servers Application
# 
#
# web.example.com
# This is for webserver which includes the proxy server and the webserver.
# Sample Configuration:
# node 'web.example.com' {
# include roles::webserver
# }
# 
#
# App Server configuration:
# This is for an app-server only. It's sole role is to create the deployment directory
# for the Go-Server and configure the open ports.
# node app01.example.com {
# include roles::app
# }
#
#




node default {
  # This is where you can declare classes for all nodes.
  # Example:
  #   class { 'my_class': }
  #include profiles::base
  #include profiles::users
  #notice("this is site.pp")
  #include profiles::apache
  
}



node 'web.example.com' {
  class {'roles::webserver':
  }
}
node 'app01.example.com' {
  class {'roles::goservers':
  }
}

node 'app02.example.com' {
  class {'roles::goservers':
  }
}


     

