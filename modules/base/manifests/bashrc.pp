class base::bashrc {

  file {'/etc/bashrc':
    ensure => 'present',
    owner  => 'root',
    group  => 'root',
    mode   => '0755',
    source => 'puppet:///modules/base/bashrc',
  }
}
