class base::hosts {


  host {'web.example.com':
    ip           => '192.168.56.101',
    host_aliases => 'web',
    comment      => 'The Web Server',
  }

  host {'app01.example.com':
    ip           => '192.168.56.102',
    host_aliases => 'app01',
    comment      => 'The Annotation App',
  }

  host {'app02.example.com':
    ip           => '192.168.56.103',
    host_aliases => 'db01',
    comment      => 'The main db server',
  }

}


