class base::motd {
  file { '/etc/motd':
    ensure => 'present',
    owner  => 'root',
    group  => 'root',
    mode   => '0444',
    source => 'puppet:///modules/base/motd'
  }
}

