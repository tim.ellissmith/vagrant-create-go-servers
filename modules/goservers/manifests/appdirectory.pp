class goservers::appdirectory (
  $owner      = $goservers::params::owner,
  $group      = $goservers::params::group,
  $server_dir = $goservers::params::server_dir,
) inherits goservers::params {
  file { $server_dir:
    ensure => present,
    owner  => $owner,
    group  => $group,
    mode   => '0500',
  }
}
