class goservers::goservers (
  $server_dir   = $goservers::params::server_dir,
  $owner        = $goservers::params::owner,
  $group        = $goservers::params::group,
  $service_name = $goservers::params::service_name,

) inherits goservers::params {
  goservers::user {$owner:
  }
  
  include goservers::appdirectory
}


