class goservers::params {
  if($::fqdn) {
    $servername = $::fqdn
  } else {
    $servername = $::hostname
  }

  $owner            = 'go'
  $group            = 'go'
  $service_name     = '/etc/nginx/sites-available'
  $server_dir       = '/go'
}
