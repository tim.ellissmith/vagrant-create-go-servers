class nginx::default_site (
    $vhost_enable = $nginx::params::vhost_enable,
    $service_name = $nginx::params::service_name,
    $package_name = $nginx::params::package_name, 
  ) 
    {

  file {"$vhost_enable/default":
    ensure  => 'absent',
    require => Package[$package_name],
    notify => Service[$service_name],
  }
}

