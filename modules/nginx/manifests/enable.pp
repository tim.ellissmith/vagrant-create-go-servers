define nginx::enable (
    $vhost_dir    = $nginx::params::vhost_dir,
    $vhost_enable = $nginx::params::vhost_enable,
    $conf_file    = $nginx::params::conf_file,
    $service_name = $nginx::params::service_name,
    $package_name = $nginx::params::package_name,
    )  {
  file {"$vhost_enable/$conf_file":
    ensure  => 'link',
    target  => "$vhost_dir/$conf_file",
    require => File[$conf_file],
    notify  => Service[$service_name],
  }
}

