class nginx::nginx (
  $package_name   = $nginx::params::package_name,
  $service_name   = $nginx::params::service_name,
  $vhost_dir      = $nginx::params::vhost_dir,
  $config_dir     = $nginx::params::conf_dir,
  $servername     = $nginx::params::servername,
  $conf_file      = $nginx::params::conf_file,
  $conf_user      = $nginx::params::conf_user,
  $app_servers    = $nginx::params::app_servers,
  $vhost_enable   = $nginx::params::vhost_enable,

) inherits nginx::params {
  include nginx::default_site
  include nginx::package
  include nginx::service
  nginx::enable {"load-balancer":
    vhost_dir    => $vhost_dir, 
    vhost_enable => $vhost_enable,
    conf_file    => $conf_file,
    service_name => $service_name,
    package_name => $package_name,
  }
  
  nginx::vhost {"load-balancer":
    servername   => $servername,
    vhost_dir    => $vhost_dir,
    app_servers  => $app_servers,
    conf_file    => $conf_file,
  }


}


