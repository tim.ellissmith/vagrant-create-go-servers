class nginx::package ($package_name = $nginx::params::package_name) {
  package {'nginx':
    name   => $package_name,
    ensure => 'present',
  }
}

