class nginx::params {
  if($::fqdn) {
    $servername = $::fqdn
  } else {
    $servername = $::hostname
  }

  $log_dir = '/var/log/nginx'
  $proxy_server = "127.0.0.1"
  $app_servers  = ["app01.example.com","app02.example.com"]
  $package_name = 'nginx'
  $service_name = 'nginx'
  $vhost_dir    = '/etc/nginx/sites-available'
  $vhost_enable = '/etc/nginx/sites-enabled'
  $conf_file    = 'load-balancer'
  $conf_user    = 'nginx'
}
