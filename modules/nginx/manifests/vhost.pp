define nginx::vhost (
  $servername,
  $vhost_dir,
  $app_servers,
  $conf_file,
)
  {
    File { mode => '0644',
    }


    file { $conf_file:
      ensure  => 'present',
      path    => "${vhost_dir}/$conf_file",
      content => template('nginx/load-balancer.erb'),
      require => Package['nginx'],
      notify  => Service['nginx'],
    }
  }

