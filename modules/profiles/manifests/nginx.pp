class profiles::nginx ($app_servers = ["app01.example.com", "app02.example.com"]) {
  class { "nginx::nginx":
    app_servers => $app_servers,
  }
}

