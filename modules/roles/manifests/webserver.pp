class roles::webserver (
  ) {
  include profiles::base
  class { "profiles::nginx":
    app_servers => $app_servers,
  }
}

