#! /bin/bash

# Start the servers
vagrant up jenkins
vagrant up web
vagrant up app01
vagrant up app02

echo "Waiting for all servers to come up!"

#Trigger a build using Jenkins
echo "Building go-application"
curl localhost:8080/job/go-webserver/build
echo Waiting for build to complete
sleep 60

#Test to see if the webserver is returning result
echo Testing to see if the web server is running...
curl -I http://localhost:8000/

if [ $? -eq 0 ]; then
        echo "WebServer is running"
else
        echo "WebServer is not running"
        exit 1
fi

#Test to see that both instances are in the load balancer
RESULT1=$(curl localhost:8000 2>/dev/null | awk -F " " '{print $NF}')
RESULT2=$(curl localhost:8000 2>/dev/null | awk -F " " '{print $NF}')

echo Testing to see if both instances are in the loadbalancer
if [ $RESULT1 != $RESULT2 ]; then
        echo "Both instances are in the load balancer"
elif [ $RESULT1 == $RESULT2 ]; then 
        echo "Only one instance seems to be in the load balancer"
else
        echo "Unknown error"
fi


